clearscreen.

stage.
wait 5.

rcs on.
sas off.
set throttle to 0.


lock EastVec to VCRS(SHIP:UP:FOREVECTOR, SHIP:North:FOREVECTOR).
set ShipDir to LOOKDIRUP(EastVec * -1, SHIP:NORTH:FOREVECTOR).

lock steering to ShipDir.
wait 15.
set throttle to 1.
set target to "Landing Target".




until addons:tr:impactpos:lng < target:geoposition:lng
{
  set ShipDir to LOOKDIRUP(EastVec * -1 +ship:north:forevector * 1.0 * (target:geoposition:lat - addons:tr:impactpos:lat) , SHIP:NORTH:FOREVECTOR).
  if addons:tr:impactpos:lng - target:geoposition:lng < 0.5
  {
    set throttle to 0.25.
  }
  wait 0.
}
set throttle to 0.

print "Boostback burn complete".

local aerosurfaces is ship:partstitled("Elevon 4").
print "Detected " + aerosurfaces:length +" total control surfaces".
local aeromods is list().
from {local i is 0.} until i=aerosurfaces:length step {set i to i+1.} do
{
  aeromods:add(aerosurfaces[i]:getmodule("modulecontrolsurface")).
  print "Detected control surface " + i. 
}


from {local i is 0.} until i=aeromods:length step {set i to i+1.} do
{
  aeromods[i]:setfield("authority limiter", 30).  
}


until ship:altitude < 30000
{
  if ship:verticalspeed > 0
  {
    lock steering to EastVec.
  }
  else
  {
    lock steering to LOOKDIRUP(ship:srfretrograde:forevector, ship:north:forevector).
  }
  wait 0.
}

print "Targeting landing site".



function sign
{
  parameter x.

  if x< 0
  {return -1.}
  else
  {return 1.}
}
function getTWR
{

  return Ship:availablethrust / (ship:mass * constant:g0).
}

function altToSpeed
{
  parameter alt.
  parameter speed is -20.

  local mul is 1.5.
  if abs(speed)>430
  {
    set mul to 2.5.
  }
  if speed>-10
  {
    return -2.
  }
  if abs(alt) > 50
  { return sign(AltError) * sqrt(AltError * (ship:availablethrustat(1)/ship:mass - constant:g0) * mul * sign(AltError)). }
  else
  {return 1 * alt.}

}

function limit
{
  parameter x, minx, maxx.
  return max(minx, min(maxx, x)).
}


local TargetAlt is 23.
SET TargetSpeed TO 0. // [m/s]
SET pSpeed TO .2.
SET iSpeed TO 0.0005.
SET SpeedInt TO 0.
local SpeedLimit is 3000.
local t is 0.
local trigger is true.

clearscreen.
lock throttle to t.

// landing targeting
until false
{
  clearscreen.
  SET rAlt TO ALT:RADAR.
  local overshootVec is v(0,0,0).
  if rAlt > 2500
  {
    set overshootVec to vxcl(ship:up:forevector, ship:srfprograde:forevector):normalized * interp(rAlt, 10000, 1000, 45, 30). // aim to lan 30m beyond target if above 300m
  }
  local landingError is v(0,0,0).
  if addons:tr:hasimpact
  {
    set landingError to (target:geoposition:position + overshootVec) - addons:tr:impactpos:position.
  }
  print "Landing Error: " + round(landingError:mag, 3) + "m" at(0,0).

  set ShipDir to ship:up:forevector + 0.001 * landingError.
  
  local mul is 0.005.

  if ship:verticalspeed>-480
  {
    set mul to 0.01.
    if rAlt<400
    {
      set mul to 0.02.
    }
    set ShipDir to ship:up:forevector + mul * landingError.
  }
  else
  {
    if rAlt<6000
    {
      set mul to 0.02.
    }
    set ShipDir to ship:srfretrograde:forevector - mul * landingError.
  }

  if rAlt < 50 and trigger
  {
    set trigger to false.
    set SpeedInt to 0.
  }
  
  // burn control
  SET AltError TO TargetAlt - rAlt.

  // Set desired speed
  SET TargetSpeed TO MAX( -SpeedLimit, MIN( SpeedLimit, altToSpeed(AltError, ship:verticalspeed) ) ).
  print "Target Vel: " + round(TargetSpeed, 3) +"m/s" at(0,1).
  print "Radar Alt: " + round(rAlt, 3) + "m" at(0,2).

  // Set throttle to achieve speed
  SET SpeedError TO TargetSpeed - SHIP:VERTICALSPEED.
  SET SpeedInt TO SpeedInt + SpeedError.
  SET SpeedInt TO MAX( -1/iSpeed, MIN( 1/iSpeed, SpeedInt) ). //Limit windup
  SET t TO SpeedError * pSpeed + SpeedInt * iSpeed.
  lock steering to LOOKDIRUP(ShipDir, ship:north:forevector).


  wait 0.
}

