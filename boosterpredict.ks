//boosterpredict.ks
clearscreen.
print "Prediction!".

function interp
{
  parameter xq, x0, x1, y0, y1.

  local yq is y0 +  ((xq-x0) / (x1-x0) ) * (y1-y0).

  return yq.
}

function collisionInfo
{
  // Returns a list with:
  // list[0] as the time until collision.
  // list[1] as the gelocation of the collision.
  // if list[0] is negative, a collision may not occur or an error may have occured.
  parameter timeUntilCollision is 0.
  local now is time.
  local curOrbit is ship:orbit.

  // naive possible collision check
  if curOrbit:periapsis > 0
  {
    return -1.
  }

  local altCollision is 0.


  local prevTimeUntilCollision is curOrbit:eta:periapsis.
  local prevAltCollision is curOrbit:periapsis.

  // Guess a time to impact to start iteration.
  // assume e < 1

  // decending
  if curOrbit:meananomalyatepoch >  180
  {
    local periTime is curOrbit:eta:periapsis.
    set timeUntilCollision to interp(0, ship:altitude, curOrbit:periapsis, 0, periTime).
  } // else ascending 
  set futurePos to positionat(ship, now+timeUntilCollision).
  set altCollision to ship:body:altitudeof(futurePos).
  //print altCollision.

  // Iterate to refine value.
  local i is 0.
  local colLocation is 0.
   until abs(altCollision) < 1 or i > 20
  {
    //print "Iteration: " + i.
    local tGuess is timeUntilCollision - altCollision / ( (prevAltCollision - altCollision)/(prevTimeUntilCollision - timeUntilCollision) ).
    set futurePos to positionat(ship, now+tGuess).

    set prevTimeUntilCollision to timeUntilCollision.
    set prevAltCollision to altCollision.

    set timeUntilCollision to tGuess.
    set colLocation to ship:body:geopositionof(futurepos).
    set altCollision to ship:body:altitudeof(futurePos) - colLocation:terrainheight.

    //print "timetill: " + round(timeUntilCollision,5).
    //print "alt: " + round(altCollision, 5).

    set i to i + 1.
  }



  return list(timeUntilCollision, colLocation).
}