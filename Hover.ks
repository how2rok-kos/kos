CLEARSCREEN.
//Start test program

RCS ON.
SAS Off.



SET StopHover TO FALSE.
SET TargetAlt TO 15000. // [m]
SET SpeedLimit TO 400. // [m/s]

SET Loc TO SHIP:GEOPOSITION.
LOCAL BoundsBox is ship:bounds.


SET pAlt TO 0.1.

SET TargetSpeed TO 0. // [m/s]
SET pSpeed TO .5.
SET iSpeed TO 0.1.
SET SpeedInt TO 0.



SET pLoc TO 25.
SET dLoc TO 150.
SET Pointing TO V(0,0,0).
SET LatError TO 0.
SET LngError TO 0.



SET t TO 0.
SET ShipDir TO SHIP:UP.
LOCK THROTTLE TO t.
LOCK STEERING TO ShipDir.
UNTIL StopHover
{
  //SET rAlt to BoundsBox:BOTTOMALTRADAR.
  SET rAlt TO ALT:RADAR.



  // Vertical control /////////////////////////////////////////////////////////
  IF rAlt < 4 AND TargetAlt < 2
  {
    UNLOCK THROTTLE.
    SET THROTTLE TO 0.
    SET StopHover TO TRUE. //Break out of loop.
  }

  IF rAlt > 50 OR TargetAlt > rAlt+5 // + 5[m] to avoid jitter
  {
    SET SpeedLimit TO 400. // [m/s]
    GEAR OFF.
  }
  ELSE
  {
    SET SpeedLimit TO 2. // [m/s]
    GEAR ON.
    SET pLoc TO 40.
  }

  IF TERMINAL:INPUT:HASCHAR
  {
    TERMINAL:INPUT:CLEAR().
    SET TargetAlt TO 0.
  }
  SET AltError TO TargetAlt - rAlt.

  // Set desired speed
  SET TargetSpeed TO MAX( -SpeedLimit, MIN( SpeedLimit, AltError * pAlt ) ).

  // Set throttle to achieve speed
  SET SpeedError TO TargetSpeed - SHIP:VERTICALSPEED.
  SET SpeedInt TO SpeedInt + SpeedError.
  SET SpeedInt TO MAX( -1/iSpeed, MIN( 1/iSpeed, SpeedInt) ). //Limit windup
  SET t TO SpeedError * pSpeed + SpeedInt * iSpeed.

  // End Vertical control /////////////////////////////////////////////////////

  // Horizontal control ///////////////////////////////////////////////////////
  SET Loc TO SHIP:GEOPOSITION.

  if HASTARGET
  {
    SET TargetLoc TO TARGET:GEOPOSITION.

    SET LatErrorOld TO LatError.
    SET LngErrorOld TO LngError.
    SET LatError TO TargetLoc:LAT - Loc:LAT.
    SET LngError TO TargetLoc:LNG - Loc:LNG.

    SET LatVel TO LatError - LatErrorOld.
    SET LngVel TO LngError - LngErrorOld.

    SET NorthError TO SHIP:NORTH:FOREVECTOR * (LatError + dLoc * LatVel).
    SET EastVec TO VCRS(SHIP:UP:FOREVECTOR, SHIP:North:FOREVECTOR).
    SET EastError TO EastVec * (LngError + dLoc * LngVel).

    SET Pointing TO SHIP:UP:FOREVECTOR.

    IF t<0
    {
      SET NorthError TO NorthError * -1.
      SET EastError TO EastError * -1.
    }

    SET Pointing TO Pointing + pLoc * (NorthError + EastError).

    SET ShipDir TO LOOKDIRUP(Pointing, SHIP:NORTH:FOREVECTOR).

  }
  ELSE
  {
    SET ShipDir to SHIP:UP.
  }


  CLEARSCREEN.
  PRINT "Throttle setting: " + t.
  PRINT "Target alt: " + TargetAlt + " m".
  PRINT "Target vSpeed: " + TargetSpeed + " m/s".
  PRINT "HASTARGET: " + HASTARGET.
  PRINT "Pointing Dir: " + ShipDir.
  WAIT 0.0.
}











//END
RCS Off.
BRAKES ON.