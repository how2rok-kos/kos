clearscreen.

print "Tower Script started".

wait 0.1.

local booster is vessel("Booster").

local hinges is ship:partstitled("G-32W Hinge").

local mod0 is hinges[0]:getmodule("moduleRoboticServoHinge").
local mod1 is hinges[1]:getmodule("moduleRoboticServoHinge").

mod0:setfield("target angle", 90).
mod1:setfield("target angle", 90).

print "set angle 90".

until booster:altitude > 500 or booster:verticalspeed<-10
{
  print "Measured Booster Altitude: " + round(booster:altitude, 3) at(0,3).
}

print "Ready to catch!".

until false
{
  print "Measured Booster Altitude: " + round(booster:altitude, 3) at(0,3).
  if booster:altitude > 115
  {
    mod0:setfield("target angle", 120).
    mod1:setfield("target angle", 120).
  }
  else if booster:altitude > 100
  {
    mod0:setfield("target angle", 140).
    mod1:setfield("target angle", 140).
  }
  else
  {
    mod0:setfield("target angle", 180).
    mod1:setfield("target angle", 180).
  }

}

