clearscreen.

print "Docking Script started".

wait until hastarget.

clearscreen.
print "Target Found!".

local targetship is 0.
local mywaypoint is v(0,0,0).

if target:istype("Orbitable")
{
  set targetship to target.
}
else if target:istype("Part")
{
  set targetship to target:ship.
}


rcs off.


lock mywaypoint to target:position + (target:facing:forevector * 10).
lock steering to lookdirup(mywaypoint, ship:north:forevector).


local relvelvec is v(0,0,0).
lock relvelvec to ship:velocity:orbit - targetship:velocity:orbit.

local targetArrow to vecdraw(
    v(0,0,0),
    mywaypoint,
    RGB(1,0,0),
    "waypoint pos",
    1,
    true,
    0.2,
    true
    ).

local relVelArrow to vecdraw(
    v(0,0,0),
    10*relvelvec,
    RGB(1,0,0),
    "rel vel",
    1,
    true,
    0.2,
    true
    ).


wait 5.
rcs on.


until brakes
{
  set targetArrow:vec to mywaypoint.
  set relVelArrow:vec to 10*relvelvec.

  print "starboard vel: " + round( ship:facing:starvector * relvelvec, 5) at(0,1).
  SET starboardRelVel TO SHIP:FACING:STARVECTOR * relVelVec.
  SET foreRelVel TO SHIP:FACING:FOREVECTOR * relVelVec.
  SET topRelVel TO SHIP:FACING:TOPVECTOR * relVelVec.



  //SET ship:control:top to -10*topRelVel.
  //SET ship:control:starboard to -10*starboardRelVel.

}


set targetArrow:show to false.
set relVelArrow:show to false.
unset targetArrow.
unset relVelArrow.

set ship:control:neutral to true.

clearscreen.
print "END OF PROGRAM".