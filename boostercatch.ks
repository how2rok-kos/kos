CLEARSCREEN.
//Start test program

RCS ON.
SAS Off.

function sign
{
  parameter x.

  if x< 0
  {return -1.}
  else
  {return 1.}
}
function getTWR
{

  return Ship:availablethrust / (ship:mass * constant:g0).
}

function altToSpeed
{
  parameter alt.
  if abs(alt) > 25
  { return sign(AltError) * sqrt(AltError * (ship:availablethrustat(1)/ship:mass - constant:g0) * 1.80 * sign(AltError)). }
  else
  {return 0.1 * alt.}

}

function limit
{
  parameter x, minx, maxx.
  return max(minx, min(maxx, x)).
}


local mygui is gui(200).
local label1 is mygui:addlabel("Booster Controls").
set label1:style:align to "center".
set label1:style:hstretch to true.

//local s is mygui:addhslider(50, 20, 50).
local bttn is mygui:addcheckbox("Landing Mode", false).

mygui:show().

SET StopHover TO FALSE.
SET TargetAlt TO 100. // [m]
SET SpeedLimit TO 3000. // [m/s]

SET Loc TO SHIP:GEOPOSITION.
SET initLoc to Loc.
LOCAL BoundsBox is ship:bounds.

local aerosurfaces is ship:partstitled("Elevon 4").
print "detected " + aerosurfaces:length +" total control surfaces".
local aeromods is list().
from {local i is 0.} until i=aerosurfaces:length step {set i to i+1.} do
{
  aeromods:add(aerosurfaces[i]:getmodule("modulecontrolsurface")).
  print "detected control surface " + i. 
}




SET TargetSpeed TO 0. // [m/s]
SET pSpeed TO .5.
SET iSpeed TO 0.1.
SET SpeedInt TO 0.



SET pLoc TO 32.
SET dLoc TO 180.
SET Pointing TO V(0,0,0).
SET LatError TO 0.
SET LngError TO 0.

local targetArrow to vecdraw(
    v(0,0,0),
    v(0,0,0),
    RGB(1,0,0),
    "waypoint pos",
    1,
    true,
    0.2,
    true
    ).



SET t TO 1.
SET ShipDir TO SHIP:UP.
LOCK THROTTLE TO t.
LOCK STEERING TO ShipDir.
stage.
wait 1.
stage.
UNTIL StopHover
{
  //SET rAlt to BoundsBox:BOTTOMALTRADAR.

  if bttn:pressed
  {
    //SET TargetAlt to s:value.
    SET TargetAlt to 29.
    if hastarget = false
    {
      set target to "Landing Target".
    }
  }
  else
  {set TargetAlt to 20000.}
  SET rAlt TO ALT:RADAR.



  // Vertical control /////////////////////////////////////////////////////////
  IF rAlt < 31 AND bttn:pressed
  {
    SET THROTTLE TO 0.
    UNLOCK THROTTLE.
    SET StopHover TO TRUE. //Break out of loop.
  }

  IF TERMINAL:INPUT:HASCHAR
  {
    TERMINAL:INPUT:CLEAR().
    //SET TargetAlt TO 0.
  }
  SET AltError TO TargetAlt - rAlt.

  // Set desired speed


  SET TargetSpeed TO MAX( -SpeedLimit, MIN( SpeedLimit, altToSpeed(AltError) ) ).

  // Set throttle to achieve speed
  SET SpeedError TO TargetSpeed - SHIP:VERTICALSPEED.
  SET SpeedInt TO SpeedInt + SpeedError.
  SET SpeedInt TO MAX( -1/iSpeed, MIN( 1/iSpeed, SpeedInt) ). //Limit windup
  SET t TO SpeedError * pSpeed + SpeedInt * iSpeed.

  // End Vertical control /////////////////////////////////////////////////////

  // Horizontal control ///////////////////////////////////////////////////////
  SET Loc TO SHIP:GEOPOSITION.

  if HASTARGET
  {
    SET TargetLoc TO TARGET:GEOPOSITION.

    set targetArrow:vec to TargetLoc:altitudeposition(ship:altitude).

    SET LatErrorOld TO LatError.
    SET LngErrorOld TO LngError.
    SET LatError TO TargetLoc:LAT - Loc:LAT.
    SET LngError TO TargetLoc:LNG - Loc:LNG.

    SET LatVel TO LatError - LatErrorOld.
    SET LngVel TO LngError - LngErrorOld.

    if rAlt>1000
    {
      SET pLoc TO 40.
      SET dLoc TO 250.
    }
    else
    {
      SET pLoc TO 80.
      SET dLoc TO 100.
    }

    SET NorthError TO SHIP:NORTH:FOREVECTOR * (LatError + dLoc * LatVel).
    SET EastVec TO VCRS(SHIP:UP:FOREVECTOR, SHIP:North:FOREVECTOR).
    SET EastError TO EastVec * (LngError + dLoc * LngVel).

    SET Pointing TO SHIP:UP:FOREVECTOR.

    IF t<-10 or ship:verticalspeed<-300
    {
      SET NorthError TO NorthError * -1.
      SET EastError TO EastError * -1.
    }
    else if ship:verticalspeed<-150
    {
      //set Pointing to ship:velocity:surface * -1.
      set Pointing to ship:up:forevector.
    }

    SET Pointing TO Pointing + pLoc * (NorthError + EastError).

    local angle to Pointing:normalized * ship:up:forevector.

    if angle<cos(45)
    {
      set Pointing to Pointing:normalized.
      local adjust is tan(45) * sqrt(1-angle^2) - angle.
      set Pointing to Pointing + adjust*ship:up:forevector.
    }

    if ship:verticalspeed > 10
    {
      if t<-10 {set Pointing to ship:up:forevector.}
      from {local i is 0.} until i=aeromods:length step {set i to i+1.} do
      {
        aeromods[i]:setfield("authority limiter", 0). 
      }
    }
    else
    {
      from {local i is 0.} until i=aeromods:length step {set i to i+1.} do
      {
        aeromods[i]:setfield("authority limiter", 30).  
      }
    }

    SET ShipDir TO LOOKDIRUP(Pointing, SHIP:NORTH:FOREVECTOR).

  }
  ELSE
  {
    SET ShipDir to SHIP:UP.
  }


  CLEARSCREEN.
  PRINT "Throttle setting: " + t.
  PRINT "Target alt: " + TargetAlt + " m".
  PRINT "Target vSpeed: " + TargetSpeed + " m/s".
  print "lat/long error: " + round(LatError,5) + "/" + round(LngError,5).
  PRINT "HASTARGET: " + HASTARGET.
  PRINT "Pointing Dir: " + ShipDir.
  PRINT "TWR: " + getTWR().
  WAIT 0.0.
}







print "Landing Complete!".



//END
RCS Off.
BRAKES ON.