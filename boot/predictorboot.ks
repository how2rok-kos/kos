wait until ship:unpacked.

switch to 0.
wait 1.
runpath("boosterpredict.ks").

local col is collisionInfo().
until false
{
  set col to collisionInfo(col[0]).
  print "Impact time " + col[0] + "s".

  wait 1.
}